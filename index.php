<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Kelas VIII IPA Semester 1</title>

  <link rel="stylesheet" href="assets/vendors/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="assets/vendors/fancybox/jquery.fancybox.min.css">
  <link rel="stylesheet" href="assets/vendors/simplebar/simplebar.css" />
  <link rel="stylesheet" href="assets/css/page.css">
  <link rel="stylesheet" href="content/style.css">

  <script type="text/javascript" async src="assets/vendors/mathjax/MathJax.js?config=AM_CHTML"></script>
</head>
<body style="background-color: #ddd;">

  <div class="container-fluid">
    <div class="row">
      <div class="col-3">
        <div class="sidebar" data-simplebar>
          <?php
          $files = scandir('./content');
          $files = array_filter($files, function($file){
            return strpos($file, '.html');
          });
          $page = $_GET['page'] ?? '0-about.html';
          ?>
          <div class="list-group" style="width: 100%;">
            <?php foreach ($files as $file): ?>
              <?php $info = pathinfo($file); ?>
              <a class="list-group-item list-group-item-action <?= $page == $info['basename'] ? 'active':''; ?>" href="?page=<?= $info['basename']; ?>">Halaman <?= $info['filename']; ?></a></li>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div class="col-9">
        <div class="page">
          <div class="page-body" data-simplebar>
            <div class="content">
              <?php
                $content = file_get_contents('./content/'.$page);
                echo $content;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <script src="assets/vendors/jquery/jquery.min.js"></script>
  <script src="assets/vendors/jquery-ui-1.12.1/jquery-ui.min.js"></script>
  <script src="assets/vendors/popper/popper.min.js"></script>
  <script src="assets/vendors/fancybox/jquery.fancybox.min.js"></script>
  <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/vendors/simplebar/simplebar.min.js"></script>
  <script src="assets/js/script.js"></script>
</body>
</html>
